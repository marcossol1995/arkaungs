package juego;

import java.awt.Color;

import entorno.Entorno;

public class Nave {
	private double x;
	private double y;
	private int longitud;
	private int altura;
	private int vidas;
	private int equipo; // -1-pegote inicial 0-ninguno 1-dispara 2-pegote

	Nave() {
		this.longitud = 80;
		this.altura = 15;
		this.x = 400;
		this.y = 580;
		this.vidas = 3;
		this.equipo = -1;
	}

	void dibujar(Entorno e) {
		e.dibujarRectangulo(this.x, this.y, this.longitud, this.altura, 0, Color.blue);
	}

	void moverIzquierda() {
		this.x -= 6;
	}

	void moverDerecha() {
		this.x += 6;
	}

	static boolean puedeMoverseIzquierda(Nave nave) {
		if ((nave.x - nave.longitud / 2) < 5) {
			return false;
		}
		return true;
	}

	static boolean puedeMoverseDerecha(Nave nave) {
		if (nave.x + nave.longitud / 2 > 808) {
			return false;
		}
		return true;
	}

	double puntaDerecha() {
		return ((this.longitud / 2) + this.x);
	}

	double puntaIzquierda() {
		return (this.x - (this.longitud / 2));
	}

	void alargar() {
		this.longitud += 40;
	}

	void disparar() {
		this.equipo = 1;
	}

	public void pegote() {
		this.equipo = 2;
	}

	public void normalizar() {
		this.equipo = 0;
	}

	public void achicar() {
		this.longitud = 80;
	}

	public void inicializar() {
		this.equipo = -1;
	}

	public void lifeUp() {
		this.vidas++;
	}

	public void lifeDown() {
		this.vidas--;
	}
	public int getEquipo() {
		return this.equipo;
	}
	public double getX() {
		return this.x;
	}
	
	public double getY() {
		return this.y;
	}

	public double getLong() {
		return this.longitud;
	}

	public double getAlt() {
		return this.altura;
	}

	public int getVidas() {
		return this.vidas;
	}

}
