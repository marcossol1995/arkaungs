package juego;

import entorno.Entorno;
import javafx.scene.paint.Color;

public class Poder {
	private int tipoDePoder; // 0-Alargar,1-Enlentecer,2-Acelerar,3-Equipar,4-Pegote,5-Vida
	private double x;
	private double y;
	private int longitud;
	private int altura;
	private java.awt.Color color;

	Poder(double x, double y, int tipoDePoder) {
		this.x = x;
		this.y = y;
		this.altura = 15;
		this.longitud = 30;
		this.tipoDePoder = tipoDePoder;
		if (this.tipoDePoder == 0) {
			this.color = java.awt.Color.yellow;
		}
		if (this.tipoDePoder == 1) {
			this.color = java.awt.Color.white;
		}
		if (this.tipoDePoder == 2) {
			this.color = java.awt.Color.blue;
		}
		if (this.tipoDePoder == 3) {
			this.color = java.awt.Color.pink;
		}
		if (this.tipoDePoder == 4) {
			this.color = java.awt.Color.green;
		}
		if (this.tipoDePoder == 5) {
			this.color = java.awt.Color.red;
		}
	}

	void dibujar(Entorno e) {
		e.dibujarRectangulo(this.x, this.y, this.longitud, this.altura, 0, this.color);
	}

	static void dibujarArreglo(Poder[] poderes, Entorno e) {
		for (int i = 0; i < poderes.length; i++) {
			if (poderes[i] != null) {
				poderes[i].dibujar(e);
			}
		}
	}

	void caer() {
		this.y += 2;
	}

	public boolean eliminarPoder() {
		return this.y > 600;
	}

	boolean tocaNave(Nave nave) {
		if (this.x + this.longitud < nave.getX()) {
			return false;
		}
		if (this.y + this.altura < nave.getY()) {
			return false;
		}
		if (this.x > nave.getX() + nave.getLong()) {
			return false;
		}
		if (this.y > nave.getY() + nave.getAlt()) {
			return false;
		}
		return true;
	}

	
	public static void limpiar(Poder[] poderes){
		for (int i = 0; i < poderes.length; i++) {
			poderes[i]=null;
		}
	}
	public int getTipoDePoder() {
		return this.tipoDePoder;
	}
}
