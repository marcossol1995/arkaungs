package juego;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;

import entorno.Entorno;
import entorno.InterfaceJuego;

public class Juego extends InterfaceJuego {

	private Entorno entorno;
	private Nave nave;
	private Pelota pelota;
	private Ladrillo[] ladrillos;
	private Poder[] poderes;
	private Bala bala;
	private Bala bala2;

	Juego() {
		this.entorno = new Entorno(this, "ArkaUngs - Grupo Apellido1 - Apellido2 -Apellido3 - V0.01", 800, 600);
		nave = new Nave();
		pelota = new Pelota(nave.getX(), nave.getY());
		ladrillos = new Ladrillo[108];
		poderes = new Poder[108];
		Random generador = new Random();
		double x = 36;
		double y = 13;
		int i = 0;
		while (y < 260) {
			ladrillos[i] = new Ladrillo(x, y);
			if (x > 740) {
				y += 30;
				x = 36;
				i++;
			} else {
				x += 68;
				i++;
			}
		}

		// Inicia el juego!
		this.entorno.iniciar();
	}

	public void tick() {
		System.gc();
		Random generador = new Random();
		this.entorno.cambiarFont("", 25, Color.BLUE);
		this.entorno.escribirTexto("Vidas restantes = " + nave.getVidas(), 5, 608);

		if (Ladrillo.quedanLadrillos(ladrillos) != true) {
			pelota = null;
			this.entorno.cambiarFont("", 25, Color.BLUE);
			this.entorno.escribirTexto("GANASTE!!!", 340, 300);
		}
		if (pelota == null && Ladrillo.quedanLadrillos(ladrillos)) {
			this.entorno.cambiarFont("", 25, Color.RED);
			this.entorno.escribirTexto("PERDEDOR!!!", 340, 300);
		}
		if (pelota != null) {
			if (pelota.getY() > 600) {
				if (nave.getVidas() == 0) {
					pelota = null;
				} else {
					nave.lifeDown();
					pelota = new Pelota(nave.getX(), nave.getY());
					nave.inicializar();
					nave.achicar();
					Poder.limpiar(poderes);
				}
			}
		}

		for (int i = 0; i < ladrillos.length; i++) {
			if (poderes[i] != null) {
				poderes[i].caer();
			}
		}
		for (int i = 0; i < poderes.length; i++) {
			if (poderes[i] != null)
				if (poderes[i].eliminarPoder())
					poderes[i] = null;
		}

		if (bala != null) {
			bala.ascender();
		}
		if (bala != null) {
			if (bala.getY() <= 0) {
				bala = null;
			}
		}
		if (bala2 != null) {
			bala2.ascender();
		}
		if (bala2 != null) {
			if (bala2.getY() <= 0) {
				bala2 = null;
			}
		}
		if (pelota != null) {
			if (!(nave.getEquipo() == 2 && pelota.tocaNaveSuperior(nave)
					|| nave.getEquipo() == -1 && pelota.tocaNaveSuperior(nave)))
				pelota.pelotaEnMovimiento();
			pelota.rebotar(nave);

			if (bala != null) {
				for (int i = 0; i < ladrillos.length; i++) {
					if (ladrillos[i] != null && bala != null && bala.tocaLadrillo(ladrillos[i])) {
						bala = null;
						if (ladrillos[i].getColor() == Color.DARK_GRAY) {
							ladrillos[i].cambiaColorDeGris();
						} else {
							if (generador.nextInt(6) == 3) {
								poderes[i] = ladrillos[i].darPoder();
							}
							ladrillos[i] = null;
						}
					}
				}
			}
		}

		if (bala2 != null) {
			for (int i = 0; i < ladrillos.length; i++) {
				if (ladrillos[i] != null && bala2 != null && bala2.tocaLadrillo(ladrillos[i])) {
					bala2 = null;
					if (ladrillos[i].getColor() == Color.DARK_GRAY) {
						ladrillos[i].cambiaColorDeGris();
					} else {
						if (generador.nextInt(6) == 3) {
							poderes[i] = ladrillos[i].darPoder();
						}
						ladrillos[i] = null;
					}
				}
			}
		}

		for (int i = 0; i < ladrillos.length && pelota != null; i++) {
			pelota.rebotarEnLadrillo(ladrillos[i]);
			if (pelota.tocaLadrillo(ladrillos[i])) {
				if (ladrillos[i].getColor() == Color.DARK_GRAY) {
					ladrillos[i].cambiaColorDeGris();
				} else {
					if (generador.nextInt(6) == 3) {
						poderes[i] = ladrillos[i].darPoder();
					}
					ladrillos[i] = null;

				}
			}
		}
		for (int i = 0; i < poderes.length; i++) {
			if (poderes[i] != null && pelota != null)
				if (poderes[i].tocaNave(nave)) {
					if (poderes[i].getTipoDePoder() == 0 && nave.getLong() < 110) {
						nave.alargar();
					} else if (poderes[i].getTipoDePoder() == 1) {
						pelota.enlentecer();
					} else if (poderes[i].getTipoDePoder() == 2) {
						pelota.acelerar();
					} else if (poderes[i].getTipoDePoder() == 3) {
						nave.disparar();
					} else if (poderes[i].getTipoDePoder() == 4) {
						nave.pegote();
					} else if (poderes[i].getTipoDePoder() == 5) {
						nave.lifeUp();
					}
					poderes[i] = null;

				}
		}
		if (this.entorno.sePresiono(this.entorno.TECLA_ESPACIO) && nave.getEquipo() == 1 
				&& bala == null && bala2 == null) {
			bala = new Bala(nave.getX() - nave.getLong() / 2, nave.getY());
			bala2 = new Bala(nave.getX() + nave.getLong() / 2, nave.getY());
		}
		if (nave.getEquipo() == 2 && pelota.tocaNaveSuperior(nave)
				&& this.entorno.sePresiono(this.entorno.TECLA_ESPACIO) && pelota != null) {
			pelota.pelotaEnMovimiento();
			if (pelota.getDireccionY() > 0)
				pelota.modificarDireccionY();
		}
		if (nave.getEquipo() == -1 && this.entorno.sePresiono(this.entorno.TECLA_ESPACIO) 
				&& pelota != null) {
			pelota.pelotaEnMovimiento();
			nave.normalizar();
			if (pelota.getDireccionY() > 0) {
				pelota.modificarDireccionY();

			}
		}
		if (this.entorno.estaPresionada(this.entorno.TECLA_DERECHA) 
				&& Nave.puedeMoverseDerecha(nave)) {
			nave.moverDerecha();
			if (nave.getEquipo() == 2 
					&& pelota.tocaNaveSuperior(nave) 
					&& pelota != null || nave.getEquipo() == -1 
					&& pelota.tocaNaveSuperior(nave) && pelota != null)
				pelota.moverDurantePegoteDerecha();
		}
		if (this.entorno.estaPresionada(this.entorno.TECLA_IZQUIERDA) 
				&& Nave.puedeMoverseIzquierda(nave)) {
			nave.moverIzquierda();
			if (nave.getEquipo() == 2 && pelota.tocaNaveSuperior(nave) 
					&& pelota != null || nave.getEquipo() == -1 
					&& pelota.tocaNaveSuperior(nave) && pelota != null) {
				pelota.moverDurantePegoteIzquierda();
			}
		}
		if (this.entorno.sePresiono('0') && nave.getLong() < 110) {
			nave.alargar();
		}
		if (this.entorno.sePresiono('1')) {
			pelota.enlentecer();
		}
		if (this.entorno.sePresiono('2')) {
			pelota.acelerar();
		}
		if (this.entorno.sePresiono('3')) {
			nave.disparar();
		}
		if (this.entorno.sePresiono('4')) {
			nave.pegote();
		}
		if (this.entorno.sePresiono('5')) {
			nave.lifeUp();
		}
		nave.dibujar(this.entorno);
		if (pelota != null) {
			pelota.dibujar(this.entorno);
		}
		if (bala != null) {
			bala.dibujar(this.entorno);
		}
		if (bala2 != null) {
			bala2.dibujar(this.entorno);
		}

		Ladrillo.dibujarArreglo(ladrillos, this.entorno);
		Poder.dibujarArreglo(poderes, this.entorno);
	}

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		Juego juego = new Juego();
	}
}
