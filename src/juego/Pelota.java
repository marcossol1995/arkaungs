package juego;

import java.awt.Color;

import entorno.Entorno;

public class Pelota {
	private int diametro;
	private double x;
	private double y;
	private double velocidadX;
	private double velocidadY;

	Pelota(double x, double y) {
		this.diametro = 15;
		this.x = x;
		this.y = y - 5;
		this.velocidadX = 3;// direccion = velocidad
		this.velocidadY = -3;// direccion = velocidad
	}

	void dibujar(Entorno e) {
		e.dibujarCirculo(this.x, this.y, this.diametro, Color.CYAN);
	}

	void rebotar(Nave nave) {
		if (tocaBordeDerechoOIzquierdo()) {
			this.velocidadX *= -1;
		}
		if (tocaBordeSuperior()) {
			this.velocidadY *= -1;
		}
		if (tocaNaveSuperior(nave)) {
			this.velocidadY *= -1;
		}
	}

	void rebotarEnLadrillo(Ladrillo ladrillo) {
		if (ladrillo != null) {
			if (tocaLadrilloInferior(ladrillo) || tocaLadrilloSuperior(ladrillo)) {
				this.velocidadY *= -1;
			}
			if (tocaLadrilloIzquierda(ladrillo) || tocaLadrilloDerecha(ladrillo)) {
				this.velocidadX *= -1;
			}
		}
	}

	boolean tocaBordeDerechoOIzquierdo() {
		return (this.x + this.diametro / 2 > 808 || this.x - this.diametro / 2 < 0);
	}

	boolean tocaBordeSuperior() {
		return (this.y + this.diametro / 2 < 20);
	}

	boolean tocaNaveSuperior(Nave nave) {
		return (this.x > nave.puntaIzquierda() 
				&& this.x < nave.puntaDerecha() 
				&& this.y - this.diametro / 2 > 560);
	}

	boolean tocaLadrilloInferior(Ladrillo ladrillo) {
		if (ladrillo != null) {
			return (this.x > ladrillo.getX() - ladrillo.getlong() / 2
					&& this.x < ladrillo.getX() + ladrillo.getlong() / 2
					&& this.y - this.diametro / 2 < ladrillo.getY() + ladrillo.getAlt() / 2)
					&& this.y - this.diametro / 2 > ladrillo.getY() + (ladrillo.getAlt() / 2) - 4;
		}
		return false;
	}

	boolean tocaLadrilloSuperior(Ladrillo ladrillo) {
		if (ladrillo != null) {
			return (this.x > ladrillo.getX() - ladrillo.getlong() / 2
					&& this.x < ladrillo.getX() + ladrillo.getlong() / 2
					&& this.y + this.diametro / 2 > ladrillo.getY() - ladrillo.getAlt() / 2
					&& this.y + this.diametro / 2 < ladrillo.getY() - (ladrillo.getAlt() / 2) + 4);
		}
		return false;
	}

	boolean tocaLadrilloIzquierda(Ladrillo ladrillo) {
		if (ladrillo != null) {
			return (this.x + this.diametro / 2 > ladrillo.getX() - ladrillo.getlong() / 2
					&& this.x + this.diametro / 2 < ladrillo.getX() - (ladrillo.getlong() / 2) + 4
					&& this.y < ladrillo.getY() + ladrillo.getAlt() / 2
					&& this.y > ladrillo.getY() - ladrillo.getAlt() / 2);
		}
		return false;
	}

	boolean tocaLadrilloDerecha(Ladrillo ladrillo) {
		if (ladrillo != null) {
			return (this.x - this.diametro / 2 < ladrillo.getX() + ladrillo.getlong() / 2
					&& this.x - this.diametro / 2 > ladrillo.getX() + (ladrillo.getlong() / 2) - 4
					&& this.y < ladrillo.getY() + ladrillo.getAlt() / 2
					&& this.y > ladrillo.getY() - ladrillo.getAlt() / 2);
		}
		return false;
	}

	boolean tocaLadrillo(Ladrillo ladrillo) {
		if (ladrillo != null) {
			return (this.tocaLadrilloDerecha(ladrillo) || this.tocaLadrilloIzquierda(ladrillo)
					|| this.tocaLadrilloInferior(ladrillo) || this.tocaLadrilloSuperior(ladrillo));
		}
		return false;
	}

	void pelotaEnMovimiento() {
		this.x += this.velocidadX;
		this.y += this.velocidadY;
	}

	void enlentecer() {
		this.velocidadX *= 0.9;
		this.velocidadY *= 0.9;
	}

	void acelerar() {
		this.velocidadX *= 1.2;
		this.velocidadY *= 1.2;
	}

	void modificarDireccionY() {
		this.velocidadY *= -1;
	}

	void moverDurantePegoteIzquierda() {
		this.x -= 6;
	}

	void moverDurantePegoteDerecha() {
		this.x += 6;
	}

	public double getY() {
		return this.y;
	}

	public double getDireccionX() {
		return this.velocidadX;
	}

	public double getDireccionY() {
		return this.velocidadY;
	}
}
