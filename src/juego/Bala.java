package juego;

import java.awt.Color;

import entorno.Entorno;

public class Bala {
	private double x;
	private double y;
	private int longitud;
	private int altura;

	Bala(double x, double y) {
		this.longitud = 5;
		this.altura = 30;
		this.x = x;
		this.y = y;
	}

	void dibujar(Entorno e) {
		e.dibujarRectangulo(this.x, this.y, this.longitud, this.altura, 0, Color.cyan);
	}

	void ascender() {
		this.y -= 3;
	}

	double getX() {
		return this.x;
	}

	double getY() {
		return this.y;
	}

	boolean tocaLadrillo(Ladrillo ladrillo) {
		if (ladrillo != null) {
			return (this.x - this.longitud / 2 > ladrillo.getX() - ladrillo.getlong() / 2
					&& this.x + this.longitud / 2 < ladrillo.getX() + ladrillo.getlong() / 2
					&& this.y - this.altura / 2 < ladrillo.getY() + ladrillo.getAlt() / 2
					&& this.y - this.altura / 2 > ladrillo.getY() + ladrillo.getAlt() / 3);
		}
		return false;
	}

}
