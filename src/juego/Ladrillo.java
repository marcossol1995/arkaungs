package juego;

import java.awt.Color;
import java.awt.Point;
import java.util.Random;

import javax.swing.ImageIcon;

import com.sun.prism.Image;

import entorno.Entorno;

public class Ladrillo {
	private double x;
	private double y;
	private int longitud;
	private int altura;
	private Color color;
	private Poder poder;

	Ladrillo(double x, double y) {
		this.longitud = 68;
		this.altura = 30;
		this.x = x;
		this.y = y;
		Random generador = new Random();
		int numeroColor = generador.nextInt(6);
		if (numeroColor == 0)
			this.color = Color.MAGENTA;
		if (numeroColor == 1)
			this.color = Color.RED;
		if (numeroColor == 2)
			this.color = Color.DARK_GRAY;
		if (numeroColor == 3)
			this.color = Color.GREEN;
		if (numeroColor == 4)
			this.color = Color.ORANGE;
		if (numeroColor == 5)
			this.color = Color.WHITE;

		this.poder = new Poder(this.x, this.y, generador.nextInt(6));

	}

	void dibujar(Entorno e) {
		e.dibujarRectangulo(this.x, this.y, this.longitud, this.altura, 0, this.color);
	}

	public Poder darPoder() {
		return this.poder;
	}

	static void dibujarArreglo(Ladrillo[] ladrillos, Entorno e) {
		for (int i = 0; i < ladrillos.length; i++) {
			if (ladrillos[i] != null) {
				ladrillos[i].dibujar(e);
			}
		}
	}

	static boolean quedanLadrillos(Ladrillo[] ladrillos) {
		int contador = 0;
		for (int i = 0; i < ladrillos.length; i++) {
			if (ladrillos[i] == null) {
				contador++;
			}
		}
		return contador != ladrillos.length;
	}

	void cambiaColorDeGris() {
		this.color = Color.GRAY;
	}

	public double getX() {
		return this.x;
	}

	public double getY() {
		return this.y;
	}

	public int getlong() {
		return this.longitud;
	}

	public int getAlt() {
		return this.altura;
	}

	public Color getColor() {
		return this.color;
	}

}
